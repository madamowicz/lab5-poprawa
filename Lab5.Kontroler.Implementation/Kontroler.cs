﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab5.Display.Contract;
using Lab5.Kontroler.Contract;
using Lab5.Display.Implementation;

namespace Lab5.Kontroler.Implementation
{
    public class Kontroler : IKontroler
    {
        public IEkran ekranik;

        public Kontroler(IEkran ekranik)
        {
            this.ekranik = ekranik;
        }

        public void Programuj(string program)
        {
            ekranik.WyświetlProgram(program);
        }

        public void ZglosZakonczenie()
        {
            Console.WriteLine("Koniec. Odbierz swoją kawę!");
        }

    }
}
