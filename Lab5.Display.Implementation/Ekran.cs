﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab5.Display.Contract;
using Lab5.DisplayForm;
using System.Windows;
using System.ComponentModel;
using System.Xaml;

namespace Lab5.Display.Implementation
{
    public class Ekran : IEkran
    {
        private DisplayViewModel ekranik;

        public Ekran()
        {
            ekranik = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                var viewModel = new DisplayViewModel();
                var form = new Form();
                form.DataContext = viewModel;
                form.Show();
                return viewModel;
            }), null);
        }

        public void WyświetlProgram(string program)
        {
            ekranik.Text = program;
        }
    }
}
